package sqli.df.tp.tp_test;

public class TpTest {
	private int barPlace;
	private int occupiedPlace;
	private int people;
	private int cocktailPrice;
	private int nbCocktail;
	private String personPay;
	private int alreadyTookCocktail; 

	public int getBarPlace() {
		return barPlace;
	}

	public void setBarPlace(int barPlace) {
		this.barPlace = barPlace;
	}

	public int getOccupiedPlace() {
		return occupiedPlace;
	}

	public void setOccupiedPlace(int occupiedPlace) {
		this.occupiedPlace = occupiedPlace;
	}

	public int getPeople() {
		return people;
	}

	public void setPeople(int people) {
		this.people = people;
	}

	/*
	 * 0 signifie que le bar est fermé
	 * 1 signifie que le bar est ouvert
	 */
	public int calculOpenBar() {
		
		int avalaiblePlace = barPlace - occupiedPlace;
		if(avalaiblePlace >= people) {
			return 1;
		} else {
			return 0; 
		}
	}
	
	public int pignonIsHappy() {
		if(alreadyTookCocktail > 0) {
			return 0;
		}
		return 1;
		
	}

	public int getCocktailPrice() {
		return cocktailPrice;
	}

	public void setCocktailPrice(int cocktailPrice) {
		this.cocktailPrice = cocktailPrice;
	}

	public int getNbCocktail() {
		return nbCocktail;
	}

	public void setNbCocktail(int nbCocktail) {
		this.nbCocktail = nbCocktail;
	}

	public String getPersonPay() {
		return personPay;
	}

	public void setPersonPay(String personPay) {
		this.personPay = personPay;
	}

	public int getAlreadyTookBeer() {
		return alreadyTookCocktail;
	}

	public void setAlreadyTookBeer(int alreadyTookCocktail) {
		this.alreadyTookCocktail = alreadyTookCocktail;
	}

}
